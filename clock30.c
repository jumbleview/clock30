/*
 * clock30.c
 *
 * Created: 9/1/2013 9:40:42 AM
 *  Author: jumbleview
 */ 


#include <avr/io.h>
#include <avr/interrupt.h>

typedef enum {
	Input_UNDEFINED,
	Input_FORWARD,
	Input_BACKWARD,
	Input_ENTER
} Input;
typedef enum {
	EncoderLock_OFF = 0,
	EncoderLock_ON
}EncoderLock;

// Index of ports 
typedef enum {
	PortIndex_B = 0, // port B
	PortIndex_C,     // port C
	PortIndex_D      // port D
}PortIndex;

typedef enum {
	Slot_0 = 0,
	Slot_1,
	Slot_2,
	Slot_3,
	Slot_4,
	Slot_5	
}SlotIndex;

typedef enum {
	SetState_UNDEFINED = 0,
	SetState_MODE,
	SetState_TIME
}SetState;

typedef enum {
	SetMode_UNDEFINED = 0,
	SetMode_RESERVED,	
	SetMode_HOURS,
	SetMode_MINUTES,
	SetMode_HOURSOFF,	
	SetMode_MINUTESOFF,
	SetMode_HOURSON,
	SetMode_MINUTESON		
}SetMode;

typedef enum {
	DarkTime_OFF=0,
	DarkTime_ON
} DarkTimeState;


typedef struct { // access to particular LED on device
	PortIndex Port;
	SlotIndex Slot;
	uint8_t CtrlBit;
}LED;

const uint8_t inOutDir[3][6] =	{ // direction of input/output operation for six time slots:  charlieplexing
									{0014,0014,0006,0006,0012,0012},	// port B
									{0366,0366,0333,0333,0355,0355},	// port C
									{0066,0066,0033,0033,0055,0055}		// port D										
								};

const LED hoursLEDs[12] =	{ // Hours dial descriptor: Port Index, Slot Number, Control Bit
								{PortIndex_C, Slot_0, 004}, // 0/12 
								{PortIndex_C, Slot_5, 010},	// 01									
								{PortIndex_C, Slot_4, 040}, // 02
								{PortIndex_C, Slot_3, 010}, // 03
								{PortIndex_C, Slot_2, 020}, // 04
								{PortIndex_C, Slot_1, 020}, // 05
								//									
								{PortIndex_C, Slot_0, 040}, // 06
								{PortIndex_C, Slot_5, 001}, // 07
								{PortIndex_C, Slot_4, 004}, // 08
								{PortIndex_C, Slot_3, 001}, // 09
								{PortIndex_C, Slot_2, 002}, // 10																																																																								
								{PortIndex_C, Slot_1, 002}  // 11
							};

const LED minutesLEDs[12] =	{ // Minutes dial descriptor: Port Index, Slot Number, Control Bit
								{PortIndex_B, Slot_0, 010}, // 00/60
								{PortIndex_D, Slot_5, 001},	// 05
								{PortIndex_D, Slot_4, 004}, // 10
								{PortIndex_D, Slot_3, 001}, // 15
								{PortIndex_D, Slot_2, 002}, // 20
								{PortIndex_D, Slot_1, 002}, // 25
								//
								{PortIndex_D, Slot_0, 004}, // 30																										
								{PortIndex_B, Slot_5, 002}, // 35
								{PortIndex_B, Slot_4, 010}, // 40								
								{PortIndex_B, Slot_3, 002}, // 45								
								{PortIndex_B, Slot_2, 004}, // 50
								{PortIndex_B, Slot_1, 004}  // 55
							};

const LED	settingsLEDs[3]={ // Settings LEDs: Port Index, Slot Number, Control Bit
								{PortIndex_D, Slot_4, 040}, // left	
								{PortIndex_D, Slot_0, 040}, // middle
								{PortIndex_D, Slot_2, 020}  // right
							};

const LED	precisionLEDs[3]=	{ // Minutes Precision LEDs: Port Index, Slot Number, Control Bit
									{PortIndex_D, Slot_5, 010},	// left
									{PortIndex_D, Slot_1, 020}, // middle
									{PortIndex_D, Slot_3, 010}  // right
								};

volatile uint8_t freshOut[3][6] =	{ // array for output signals 
										{0,0,0,0,0,0},  // port B
										{0,0,0,0,0,0},  // port C
										{0,0,0,0,0,0},	// port D																
									};

#define pinIn(port,bit) \
	((DDR##port &= ~(1 << (DD##port##bit))),\
	(PORT##port |= (1 << (PORT##port##bit))),\
	(PIN##port & (1 << (PORT##port##bit))))

volatile SetMode setMode=0;
volatile SetState setModeState = SetState_UNDEFINED;

volatile int8_t hours=0;
volatile int8_t minutes=0;
volatile int8_t seconds=0;

volatile int8_t hourOff = 23;	// time to turn light off at night
volatile int8_t minuteOff = 45;

volatile int8_t hourOn = 6;		// time to turn light on in the morning
volatile int8_t minuteOn = 30;

volatile uint8_t	pulses=0;
volatile uint8_t	clockTick=0;

volatile uint8_t aLevelOld;
volatile uint8_t bLevelOld;

volatile uint8_t aLevel;
volatile uint8_t bLevel;

volatile uint8_t pushSignal;
volatile uint8_t oldPush;
volatile EncoderLock lock=EncoderLock_OFF;

volatile uint8_t forward=0;
volatile uint8_t backward=0;

volatile uint8_t oldForward=0;
volatile uint8_t oldBackward=0;

volatile DarkTimeState dt = DarkTime_OFF;

void inActivate()
{
	aLevelOld = pinIn(D,7);
	bLevelOld = pinIn(B,0);
	pushSignal = pinIn(D,6);
	lock = EncoderLock_OFF;
}

int8_t incSeconds()
{
	seconds++;
	if (seconds >= 60) {
		seconds = 0;
		return 1;
	}
	return 0;
}

int8_t countMinutes( volatile int8_t * min, int8_t delta)
{
	*min+=delta;
	if (*min>=60) {
		*min = 0;
		return 1;
	}
	if (*min<0) {
		*min = 59;
		return -1;
	}
	return 0;
}

void countHours(volatile int8_t * hour, int8_t delta)
{
	*hour+=delta;
	if (*hour>=24) {
		*hour = 0;
	}
	if (*hour<0)  {
		*hour = 23;
	}
}

void changeTime()
{
	int8_t n = incSeconds();
	n = countMinutes(&minutes, n);
	countHours(&hours, n);
}

void countMode(int8_t delta)
{
	setMode +=delta;
	if (setMode > 7) {
		setMode = 0;
	}
	if (setMode <0) {
		setMode = 7;
	}
}

void showMinutes(int8_t  mint, DarkTimeState dt)
{
	LED current;
	int8_t b = mint/5;
	int8_t m = mint%5;
	if (m > 2) {
		b++;
		if (b==12)	{
			b = 0;
		}	
	}
	int8_t	ix;	
	for ( ix = 0; ix != 12; ix++) {
		current = minutesLEDs[ix];
		if ( (dt == DarkTime_OFF) && (ix <= b)) {
			freshOut[current.Port][current.Slot] |= current.CtrlBit; // turn it on
		} else {
			freshOut[current.Port][current.Slot] &= ~current.CtrlBit; // turn it off
		}
	}
	for ( ix = 0; ix != 3; ix++) {
		current = precisionLEDs[ix];
		switch (ix) {
			case 0:
			{
				if ((dt == DarkTime_OFF) && ((m == 1) || (m == 2))) {
					freshOut[current.Port][current.Slot] |= current.CtrlBit; // turn it on					
				} else {
					freshOut[current.Port][current.Slot] &= ~current.CtrlBit; // turn it off
				}
			}
			break;
			case 1:
			{
				if ((dt == DarkTime_OFF) && ((m==2) || (m==3)) ) {
					freshOut[current.Port][current.Slot] |= current.CtrlBit; // turn it on
				} else {
					freshOut[current.Port][current.Slot] &= ~current.CtrlBit; // turn it off
				}
			}
			break;
			case 2:
			{
				if ((dt == DarkTime_OFF) && ((m==3) || (m==4)) ) {
					freshOut[current.Port][current.Slot] |= current.CtrlBit; // turn it on					
				} else {
					freshOut[current.Port][current.Slot] &= ~current.CtrlBit; // turn it off						
				}
			}
			break;
		}		
	}
}

void showHours(int8_t hour,  int8_t minute, DarkTimeState dt)
{
	LED current;
	int8_t hr = hour;
	if (minute > 57) {
		hr +=1;	
		if (hr>=24) {
			hr = 0;
		}			
	}	
	if (hour >=12) {
		hr -=12;
	}
	for (int8_t	ix = 0; ix != 12; ix++) {
		current = hoursLEDs[ix];
		if ( (dt == DarkTime_ON) || ((hour == 0) || (ix > hr) || (hour<12 && ix != hr)) ) {
			freshOut[current.Port][current.Slot] &= ~current.CtrlBit;	// turn it off			
		} else {
			freshOut[current.Port][current.Slot] |= current.CtrlBit;	// turn it on
		}
	}
}

void showSetMode()
{
	LED current;
	for ( int8_t ix = 0; ix != 3; ix++) {
		current = settingsLEDs[ix];
		int8_t odd = setMode%2;
		switch (ix) {
			case 0:
			{
				if (odd == 1) {
					freshOut[current.Port][current.Slot] |= current.CtrlBit; // turn it on
				} else {
					freshOut[current.Port][current.Slot] &= ~current.CtrlBit; // turn it off
				}
			}
			break;
			case 1:
			{
				if ((setMode==2) || (setMode==3) || (setMode>5)) {
					freshOut[current.Port][current.Slot] |= current.CtrlBit; // turn it on
				} else {
					freshOut[current.Port][current.Slot] &= ~current.CtrlBit; // turn it off
				}
			}
			break;
			case 2:
			{
				if ((setMode >= 4)) {
					freshOut[current.Port][current.Slot] |= current.CtrlBit; // turn it on
				} else {
					freshOut[current.Port][current.Slot] &= ~current.CtrlBit; // turn it off
				}
			}
			break;
		}
	}	
}

void activateTimer0()
{
	cli();
	pulses = 0;
	TCCR0A=0; // Normal operation
	TCCR0B=3; // f divider 64  : about 2 ms for interrupt ...
	// for system clock f= 8Mhz (CKDIV8 cleared)
	TIMSK0 = 1;
	sei();
}

void activateTimer2()
{
	cli();
	ASSR = 0x20;
	TCCR2A=0; // Normal operation
	//TCCR2B=3; // external source, 4 interrupts per second for 32768 oscillator
	TCCR2B=2; // external source, 16 interrupts per second for 32768 oscillator
	TIMSK2 = 1;
	sei();
}

void initializeEnter()
{
	forward = 0;
	backward = 0;
	oldForward = forward;
	oldBackward = backward;
	inActivate();
}

void light(uint8_t slot)
{
	DDRB = inOutDir[PortIndex_B][slot];
	PORTB = freshOut[PortIndex_B][slot];
//
	DDRC = inOutDir[PortIndex_C][slot];
	PORTC = freshOut[PortIndex_C][slot];
//
	DDRD = inOutDir[PortIndex_D][slot];
	PORTD = freshOut[PortIndex_D][slot];
}

Input checkInput()
{
	Input ret = Input_UNDEFINED;
	if(pushSignal == 0 && pushSignal != oldPush) { // Enter pressed
		ret = Input_ENTER;
	} else {
		if((forward - oldForward) >= 1)	{
			ret = Input_FORWARD;
		} else if((backward - oldBackward) >= 1) {
			ret = Input_BACKWARD;
		}
	}
	oldForward = forward;
	oldBackward = backward;
	oldPush = pushSignal;
	return ret;
}

SetMode ProcessInput(Input inputCode)
{
	if (inputCode == Input_UNDEFINED) {
		return setMode;
	}
	if (inputCode == Input_ENTER) {
		if (setModeState == SetState_UNDEFINED) {
				setModeState = SetState_MODE;
				setMode = SetMode_RESERVED;
			} else if (setModeState == SetState_MODE) {
				setModeState = SetState_TIME;
			}  else {// todo: activate seconds count here ..
				setModeState = SetState_UNDEFINED;
				setMode = SetMode_UNDEFINED;
		}
		return setMode;
	}
	int8_t delta=1;
	if (inputCode == Input_BACKWARD) {
		delta = -1;
	}
	if (setModeState == SetState_MODE) {
		countMode(delta);
		} else if (setModeState == SetState_TIME) {
		switch (setMode) {
			case SetMode_HOURS:
				countHours(&hours, delta);
				break;
			case SetMode_MINUTES:
				countMinutes(&minutes, delta);
				break;
			case SetMode_HOURSOFF:
				countHours(&hourOff, delta);
				break;
			case SetMode_MINUTESOFF:
				countMinutes(&minuteOff, delta);
				break;
			case SetMode_HOURSON:
				countHours(&hourOn, delta);						
				break;
			case SetMode_MINUTESON:
				countMinutes(&minuteOn, delta);
				break;
			default:
				break;
		}
	}
	return setMode;	
}

DarkTimeState GetDarkTime()
{
	uint8_t isOnAfterOff = 0;
	if ( (hourOn > hourOff) || ((hourOn == hourOff) && (minuteOn > minuteOff)) ) {
		isOnAfterOff = 1;
	}
	if (isOnAfterOff) {
		if ( ( (hours > hourOff) ||  ( (hours == hourOff) && (minutes > minuteOff) )  )  &&
			 ( (hours < hourOn ) ||  ( (hours == hourOn ) && (minutes < minuteOn ) )  )   ) {
			return DarkTime_ON;
		}
				
	} else 	{
		if ( (hours > hourOff) || ((hours == hourOff) && (minutes > minuteOff)) || 
			 (hours < hourOn) ||  ((hours == hourOn) && (minutes < minuteOn)) )	{
			return DarkTime_ON;
		} 
	}
	return DarkTime_OFF;
}

ISR(TIMER0_OVF_vect)
{
	light(pulses);		
	pulses++;
	if (pulses > 5)	{
		pulses = 0;
	}
//  Precessing of encoder signals and enter button..
	aLevel = pinIn(D,7);
	bLevel = pinIn(B,0);
	if (lock == EncoderLock_OFF) {
		if ((aLevelOld != 0) && (aLevel == 0))	{
			lock  = EncoderLock_ON;
			if (bLevel == 0) {
				forward++;
				} else {
				backward++;
			}
		}
	}
	if (bLevel != bLevelOld) {
		lock  = EncoderLock_OFF;
	}
	aLevelOld = aLevel;
	bLevelOld = bLevel;
	pushSignal = pinIn(D,6);
}


ISR(TIMER2_OVF_vect)
{
	clockTick++;
	if (clockTick == 16) {
		clockTick = 0;
		changeTime();
		dt = GetDarkTime();		
	}
	Input inputCode = checkInput();
	SetMode mode = ProcessInput(inputCode);
	switch (mode) {
		case SetMode_HOURSOFF:
		case SetMode_MINUTESOFF:
			showHours(hourOff, minuteOff, DarkTime_OFF);
			showMinutes(minuteOff, DarkTime_OFF);
			break;			
		case SetMode_HOURSON:
		case SetMode_MINUTESON:
			showHours(hourOn, minuteOn, DarkTime_OFF);
			showMinutes(minuteOn, DarkTime_OFF);
			break;
		case SetMode_HOURS:
		case SetMode_MINUTES:
			showHours(hours, minutes, DarkTime_OFF);
			showMinutes(minutes, DarkTime_OFF);
			break;
		default:
			showHours(hours, minutes, dt);
			showMinutes(minutes, dt);
			break;		
	}
	showSetMode();		
}

int main(void)
{
	activateTimer0();
	activateTimer2();
	initializeEnter();
    for(;;) {
    }
}